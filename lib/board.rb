class Board

  attr_accessor :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10) {Array.new(10)}
  end

  def populate_grid(num=2)
    get_random_pos(num).each do |pos|
      self[pos] = :s
    end
  end

  def place_random_ship
    raise 'Error' if full?
    populate_grid(1)
  end

  def get_random_pos(num_pos)
    random_pos = []
    until random_pos.count == num_pos
      y = rand(@grid.length)
      x = rand(@grid.length)
      random_pos << [y, x] unless random_pos.include?([y, x])
    end

    random_pos

  end

  def count
    @grid.flatten.count(:s)
  end

  def empty?(pos = nil) #this must
    if pos
      self[pos].nil?
    else
      count == 0
    end
  end

  def full?
    return false if @grid.flatten.include?(nil)
    true
  end

  def [](pos) #this acts on self NOT grid
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end


  def in_range?(pos)
    y = pos.first
    x = pos.last

    return true if x.between?(0, @grid.length) && y.between?(0, @grid.length)
    false

  end

  def won?
    return true unless @grid.flatten.include?(:s)
    false
  end
end
