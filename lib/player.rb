

class HumanPlayer

  def initialize(name)
    @name = name
  end

  def get_play
    p 'Enter a position!'
    gets.chomp.split.(',').map(:to_i)
  end
end
