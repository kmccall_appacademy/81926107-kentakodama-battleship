
require_relative 'board'
require_relative 'player'

class BattleshipGame
  attr_accessor :player, :board

  def initialize(player, board = 'default')
    @player = player
    if board == 'default'
      @board = Board.default_grid
    else
      @board = board
    end
  end

  def play_turn
    position = @player.get_play
    attack(position)

    @board.populate_grid(1) if count == 0 && !@board.full?
  end

  def count
    @board.count
  end

  def attack(pos)
    #@board[pos] = :m if @board.empty?(pos)
    @board[pos] = :x if @board.empty?(pos)
  end

  def game_over?
    true if @board.won?
  end
end
